import { useState } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const dragonsRoute = "https://localhost:3000/api/dragons";

  const [name, setName] = useState("");
  const [type, setDragonType] = useState("");
  const [size, setSize] = useState("");

  const [dragonsList, setDragonsList] = useState([]);

  // Add dragon POST request
  const addDragon = () => {
    axios.post(dragonsRoute, {
      name: name,
      type: type,
      size: size,
    });
  };

  // Get dragons GET request
  const getDragons = () => {
    axios.get(dragonsRoute).then((response) => {
      console.log(response);
      setDragonsList(response.data);
    });
  };

  return (
    <div className="App">
      <div className="title">
        <h2>Dragon Database Application</h2>
        <p>Dragon Information</p>
      </div>
      <div className="input-information">
        <label>Name:</label>
        <input
          type="text"
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <label>Dragon Type:</label>
        <input
          type="text"
          onChange={(event) => {
            setDragonType(event.target.value);
          }}
        />
        <label>Size:</label>
        <input
          type="text"
          onChange={(event) => {
            setSize(event.target.value);
          }}
        />
        <button onClick={addDragon}>Add Dragon</button>
        <hr width="100%" height="1px" />
        <div className="dragonButton" onClick={getDragons}>
          <button>Show Dragons</button>
        </div>
        <div className="dragonsList">
          <h2>Dragons</h2>
          {dragonsList.map((val, key) => {
            return (
              <div className="dragonItem">
                <hr width="100%" height="1px" />
                <h3>
                  Name: {val.name}, Dragon Type: {val.type}, Size: {val.size}
                </h3>
                <hr width="100%" height="1px" />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default App;
